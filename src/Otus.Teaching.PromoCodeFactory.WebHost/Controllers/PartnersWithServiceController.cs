﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{   
    [ApiController]
    [Route("api/v2/[controller]")]
    public class PartnersWithServiceController : ControllerBase
    {
        private IPartnerService _partnerService;
        public PartnersWithServiceController(IPartnerService partnerService)
        {
            _partnerService = partnerService;
        }

        [HttpGet]
        public async Task<ActionResult<List<PartnerResponseDto>>> GetPartnersAsync()
        {
            var response = await _partnerService.GetPartnersAsync();

            return Ok(response);
        }

        [HttpGet("{id}/limits/{limitId}")]
        public async Task<ActionResult<PartnerPromoCodeLimit>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            try
            {
                var response = await _partnerService.GetPartnerLimitAsync(id, limitId);
                return Ok(response);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDto request)
        {
            try
            {
                var newLimitId = await _partnerService.SetPartnerPromoCodeLimitAsync(id, request);

                return CreatedAtAction(nameof(GetPartnerLimitAsync), new {id = id, limitId = newLimitId.ToString()},
                    null);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            catch (ChangePartnerLimitException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            try{
                await _partnerService.CancelPartnerPromoCodeLimitAsync(id);

                return NoContent();
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            catch (ChangePartnerLimitException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
