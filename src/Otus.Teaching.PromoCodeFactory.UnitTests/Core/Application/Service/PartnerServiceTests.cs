﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Application.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Application.Service
{
    public class PartnerServiceTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProviderMock;
        private readonly IPartnerService _partnerService;

        public PartnerServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _currentDateTimeProviderMock = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
            _partnerService = fixture.Build<PartnerService>().Create();
        }

        [Fact]
        public async void  GetPartnersAsync()
        {
            //Arrange
            var partnerList = PartnerBuilder.CreatePartners();
            _partnersRepositoryMock.Setup(rep => rep.GetAllAsync()).ReturnsAsync(partnerList);

            //Act
            var result = await _partnerService.GetPartnersAsync();

            //Assert
            result.Should()
                .NotBeEmpty()
                .And.HaveCount(2)
                .And.ContainItemsAssignableTo<PartnerResponseDto>();
        }


        /// <summary>
        /// 1. Если партнер не найден, то также нужно выдать ошибку EntityNotFoundException;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnEntityNotFoundException()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            var requestDto = PartnerBuilder.CreateSetPartnerPromoCodeLimitRequestDto();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            Func<Task<Guid>> act = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partnerId, requestDto);

            // Assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        /// <summary>
        /// 2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку ChangePartnerLimitException;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsChangePartnerLimitException()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var requestDto = PartnerBuilder.CreateSetPartnerPromoCodeLimitRequestDto();
            Partner partner = PartnerBuilder.CreateBasePartner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            Func<Task<Guid>> act = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partnerId, requestDto);

            // Assert
            await act.Should().ThrowAsync<ChangePartnerLimitException>();
        }

        /// <summary>
        /// 3.1 Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ExistsActivLimit_SetZeroIssuedPromoCodes()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var requestDto = PartnerBuilder.CreateSetPartnerPromoCodeLimitRequestDto();
            Partner partner = PartnerBuilder.CreateBasePartner();
            partner.NumberIssuedPromoCodes = 60;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnerService.SetPartnerPromoCodeLimitAsync(partnerId, requestDto);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        /// <summary>
        /// 3.2 Если текущий лимит закончился, то  количество выданных промокодов NumberIssuedPromoCodes не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NotExistsActivLimit_IssuedPromoCodesWithoutChange()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var requestDto = PartnerBuilder.CreateSetPartnerPromoCodeLimitRequestDto();
            Partner partner = PartnerBuilder.CreateBasePartner().WithNotActiveLimit();
            partner.NumberIssuedPromoCodes = 60;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnerService.SetPartnerPromoCodeLimitAsync(partnerId, requestDto);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(60);
        }

        /// <summary>
        /// 4. При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ExistsActivLimit_CanselActivLimit()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var requestDto = PartnerBuilder.CreateSetPartnerPromoCodeLimitRequestDto();
            Partner partner = PartnerBuilder.CreateBasePartner();
            partner.NumberIssuedPromoCodes = 60;

            var targetLimit = partner.PartnerLimits.First();
            DateTime cancelDate = new DateTime(2020, 09, 9);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            _currentDateTimeProviderMock.Setup(x => x.CurrentDateTime)
                .Returns(cancelDate);

            // Act
            var result = await _partnerService.SetPartnerPromoCodeLimitAsync(partnerId, requestDto);

            // Assert
            targetLimit.CancelDate.Should().Be(cancelDate);
        }

        /// <summary>
        /// 5. Лимит должен быть больше 0;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ZeroLimit_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var requestDto = PartnerBuilder.CreateSetPartnerPromoCodeLimitRequestDto();
            requestDto.Limit = 0;
            Partner partner = PartnerBuilder.CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            Func<Task<Guid>> act = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partnerId, requestDto);

            // Assert
            await act.Should().ThrowAsync<ChangePartnerLimitException>();
        }

        /// <summary>
        /// 6. Нужно убедиться, что сохранили новый лимит в базу данных
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CreatedNewLimitToRepository()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var requestDto = PartnerBuilder.CreateSetPartnerPromoCodeLimitRequestDto();
            Partner partner = PartnerBuilder.CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnerService.SetPartnerPromoCodeLimitAsync(partnerId, requestDto);

            // Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }
    }
}
