﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerBuilder
    {
        public static Partner CreateBasePartner()
        {
            return new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };
        }

        public static Partner WithNotActiveLimit(this Partner partner)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
            partner.PartnerLimits.Add(new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("c9bef066-3c5a-4e5d-9cff-bd54479f075e"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 10, 9),
                CancelDate = new DateTime(2020, 8, 9),
                Limit = 100
            });

            return partner;
        }

        public static Partner WithOnlyOneActiveLimit(this Partner partner)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
            partner.PartnerLimits.Add(new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 10, 9),
                Limit = 100
            });

            return partner;
        }

        public static SetPartnerPromoCodeLimitRequest CreatePartnerPromoCodeLimitRequest()
        {
            var promoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = new DateTime(2021, 12, 25),
                Limit = 250
            };

            return promoCodeLimitRequest;
        }

        public static IEnumerable<Partner> CreatePartners()
        {
            IEnumerable<Partner> partners = new List<Partner>()
            {
                new Partner()
                {
                    Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                    Name = "Суперигрушки",
                    IsActive = true,
                    PartnerLimits = new List<PartnerPromoCodeLimit>()
                    {
                        new PartnerPromoCodeLimit()
                        {
                            Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                            CreateDate = new DateTime(2020, 07, 9),
                            EndDate = new DateTime(2020, 10, 9),
                            Limit = 100
                        }
                    }
                },

                new Partner()
                {
                    Id = Guid.Parse("0DCC0584-20AF-4B29-80E7-F8C535CF2934"),
                    Name = "Детский мир",
                    IsActive = true,
                    PartnerLimits = new List<PartnerPromoCodeLimit>()
                    {
                        new PartnerPromoCodeLimit()
                        {
                            Id = Guid.Parse("14768BDD-DF31-491A-ADD4-B8863E61ABC8"),
                            CreateDate = new DateTime(2020, 07, 9),
                            EndDate = new DateTime(2021, 10, 9),
                            Limit = 90
                        }
                    }
                }
            };

            return partners;
        }

        public static SetPartnerPromoCodeLimitRequestDto CreateSetPartnerPromoCodeLimitRequestDto()
        {
            var promoCodeLimitRequestDto = new SetPartnerPromoCodeLimitRequestDto()
            {
                EndDate = new DateTime(2021, 12, 25),
                Limit = 250
            };

            return promoCodeLimitRequestDto;
        }
    }
}
